<?php

namespace Test\Unit;

use PHPUnit\Framework\TestCase;
use Samy\Paraphrase\AbstractParaphrase as ParaphraseAbstractParaphrase;

class AbstractParaphrase extends TestCase
{
    protected $paraphrase;


    protected function assertLanguage($Language): void
    {
        $this->assertInstanceOf(ParaphraseAbstractParaphrase::class, $this->paraphrase->withLanguage($Language));
        $this->assertSame($Language, $this->paraphrase->getLanguage());
    }

    protected function assertMode($Mode): void
    {
        $this->assertInstanceOf(ParaphraseAbstractParaphrase::class, $this->paraphrase->withMode($Mode));
        $this->assertSame($Mode, $this->paraphrase->getMode());
    }

    protected function assertText($TextActual, $TextExpect): void
    {
        $this->assertInstanceOf(ParaphraseAbstractParaphrase::class, $this->paraphrase->withText($TextActual));
        $this->assertSame($TextExpect, $this->paraphrase->getText());
    }
}
