# Paraphrase Instance

Simple Paraphrase Implementation.

---

## Tinq AI

Implementation Tinq AI instance.

```php
$paraphrase = new TinqAi($apikey);
```

### withTier

Return an instance with provided tier.

```php
$paraphrase = $paraphrase->withTier($tier);
```

### getTier

Retrieve provided tier.

```php
$tier = $paraphrase->getTier();
```

### withUnique

Return an instance with online plagiarism tests. Feature available for Pro users and up.

```php
$paraphrase = $paraphrase->withUnique(true);
```

### getUnique

Retrieve provided unique feature.

```php
$is_unique = $paraphrase->getUnique();
```

### getCharactersLeft

Retrieve characters left usage.

```php
$characters_left = $paraphrase->getCharactersLeft();
```

### getRequestsLeft

Retrieve requests left usage.

```php
$requests_left = $paraphrase->getRequestsLeft();
```

### getCreditsLeft

Retrieve credits left usage.

```php
$credits_left = $paraphrase->getCreditsLeft();
```
