<?php

namespace Samy\Paraphrase;

/**
 * Describes Paraphrase interface.
 */
interface ParaphraseInterface
{
    /**
     * Return an instance with provided language.
     *
     * @param[in] string $Language Language code
     *
     * @return static
     */
    public function withLanguage(string $Language): self;

    /**
     * Retrieve provided language.
     *
     * @return string
     */
    public function getLanguage(): string;

    /**
     * Retrieve supported languages.
     *
     * @return array<array<string, string>>
     */
    public function getSupportedLanguages(): array;


    /**
     * Return an instance with provided mode.
     *
     * @param[in] int $Mode Mode id
     *
     * @return static
     */
    public function withMode(int $Mode): self;

    /**
     * Retrieve provided mode.
     *
     * @return string
     */
    public function getMode(): int;

    /**
     * Retrieve supported modes.
     *
     * @return array<array<string, mixed>>
     */
    public function getSupportedModes(): array;


    /**
     * Return an instance with provided text.
     *
     * @param[in] string $Text the original text
     *
     * @return static
     */
    public function withText(string $Text): self;

    /**
     * Retrieve provided text.
     *
     * @return string
     */
    public function getText(): string;


    /**
     * Retrieve paraphrased text.
     *
     * @return string
     */
    public function paraphrase(): string;
}
