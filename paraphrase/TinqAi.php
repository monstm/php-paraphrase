<?php

namespace Samy\Paraphrase;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Samy\Log\Syslog;

/**
 * Simple Tinq AI implementation.
 */
class TinqAi extends AbstractParaphrase
{
    /** describe api key */
    protected $api_key = "";

    /** describe characters left usage */
    protected $characters_left = 0;

    /** describe requests left usage */
    protected $requests_left = 0;

    /** describe credits left usage */
    protected $credits_left = 0;

    /** describe unique feature */
    protected $unique = false;

    /** describe tier */
    protected $tier = 0;

    /** describe characters request limit */
    protected $characters_request = 0;


    /** describe constant free tier */
    public const TIER_FREE        = 0;

    /** describe constant starter tier */
    public const TIER_STARTER    = 1;

    /** describe constant pro tier */
    public const TIER_PRO        = 2;

    /** describe constant ultra tier */
    public const TIER_ULTRA        = 3;

    /** describe constant scale tier */
    public const TIER_SCALE        = 4;


    /** describe constant normal mode */
    public const MODE_NORMAL    = 0;

    /** describe constant standard mode */
    public const MODE_STANDARD    = 1;

    /** describe constant fluent mode */
    public const MODE_FLUENT    = 2;

    /** describe constant creative mode */
    public const MODE_CREATIVE    = 3;


    /**
     * TinqAi construction.
     *
     * @param[in] string $ApiKey Tinq AI API Key
     *
     * @return void
     */
    public function __construct(string $ApiKey)
    {
        $this->api_key = $ApiKey;

        $this->characters_left = -1;
        $this->requests_left = -1;
        $this->credits_left = -1;
        $this->unique = false;

        $this->withTier(self::TIER_FREE);
    }


    /**
     * Return an instance with provided tier.
     *
     * @param[in] int $UniqueThe unique feature
     *
     * @return static
     */
    public function withTier(int $Tier): self
    {
        $this->tier = $Tier;

        switch ($this->tier) {
            case self::TIER_STARTER:
                $this->characters_request = 500;
                break;
            case self::TIER_PRO:
            case self::TIER_ULTRA:
                $this->characters_request = 1000;
                break;
            case self::TIER_SCALE:
                $this->characters_request = 5000;
                break;
            default:
                $this->characters_request = 400;
                break;
        }

        return $this;
    }

    /**
     * Retrieve provided tier.
     *
     * @return int
     */
    public function getTier(): int
    {
        return $this->tier;
    }


    /**
     * Return an instance with online plagiarism tests. Feature available for Pro users and up.
     *
     * @param[in] bool $Unique The unique feature
     *
     * @return static
     */
    public function withUnique(bool $Unique): self
    {
        $this->unique = $Unique;

        return $this;
    }

    /**
     * Retrieve provided unique feature.
     *
     * @return bool
     */
    public function getUnique(): bool
    {
        return $this->unique;
    }


    /**
     * Retrieve characters left usage.
     *
     * @return int
     */
    public function getCharactersLeft(): int
    {
        return $this->characters_left;
    }

    /**
     * Retrieve requests left usage.
     *
     * @return int
     */
    public function getRequestsLeft(): int
    {
        return $this->requests_left;
    }

    /**
     * Retrieve credits left usage.
     *
     * @return int
     */
    public function getCreditsLeft(): int
    {
        return $this->credits_left;
    }


    /**
     * Retrieve supported languages.
     *
     * @return array<array<string, string>>
     */
    public function getSupportedLanguages(): array
    {
        return array(
            array("code" => "en", "description" => "English"),
            array("code" => "fr", "description" => "Francais"),
            array("code" => "es", "description" => "Espanol"),
            array("code" => "pt", "description" => "Portuguese")
        );
    }

    /**
     * Retrieve supported modes.
     *
     * @return array<array<string, mixed>>
     */
    public function getSupportedModes(): array
    {
        return array(
            array(
                "code" => self::MODE_NORMAL,
                "description" => "The normal mode allows for simple rephrasing, with word changing."
            ),
            array(
                "code" => self::MODE_STANDARD,
                "description" => "The standard mode allows a relatively simple rephrasing" .
                    " with sentence structure changes while keeping the context of the input." .
                    " This is great for bypassing certain plagiarism checkers."
            ),
            array(
                "code" => self::MODE_FLUENT,
                "description" => "The fluent mode allows for more subtle rephrasing," .
                    " with more sophisticated words. This is good for rewriting scientific or technical content."
            ),
            array(
                "code" => self::MODE_CREATIVE,
                "description" => "Creative content is great when rewriting blog posts " .
                    "or other work that may require less precision, but more freedom for the A.I to make suggestions."
            )
        );
    }


    /**
     * Retrieve paraphrased text.
     *
     * @return string
     */
    public function paraphrase(): string
    {
        $ret = array();

        $lang = (in_array($this->language, array("en", "fr", "es", "pt")) ? $this->language : "en");
        $unique = ($this->unique ? "true" : "false");

        switch ($this->mode) {
            case self::MODE_STANDARD:
                $mode = "standard";
                break;
            case self::MODE_FLUENT:
                $mode = "fluent";
                break;
            case self::MODE_CREATIVE:
                $mode = "creative";
                break;
            default:
                $mode = "normal";
                break;
        }

        foreach ($this->splitTextCharacter($this->characters_request) as $text) {
            $content = http_build_query(array(
                "text" => $text,
                "lang" => $lang,
                "unique" => $unique,
                "mode" => $mode
            ));

            $response = $this->request(
                "POST",
                "https://tinq.ai/api/v1/rewrite",
                array(
                    "Accept" => "application/json",
                    "Authorization" => "Bearer " . $this->api_key,
                    "Content-Type" => "application/x-www-form-urlencoded",
                    "Content-Length" => strlen($content)
                ),
                $content
            );

            $result = $this->parseResponse($response);
            if ($result != "") {
                array_push($ret, $result);
            }
        }

        return implode(" ", $ret);
    }

    /**
     * parse PSR-7 response interface.
     *
     * @return string
     */
    private function parseResponse(ResponseInterface $ResponseInterface): string
    {
        $ret = "";
        $log = new Syslog();

        try {
            $body = $ResponseInterface->getBody();

            $body->rewind();
            $content = $body->getContents();
            $body->rewind();

            if ($ResponseInterface->getStatusCode() == 200) {
                $json = @json_decode($content, true);

                if (is_array($json)) {
                    $success = ($json["success"] ?? false);

                    if ($success) {
                        $ret = ($json["paraphrase"] ?? "");
                        $usage = ($json["usage"] ?? array());

                        if (isset($usage["characters_left"]) && is_int($usage["characters_left"])) {
                            $this->characters_left = $usage["characters_left"];
                        }

                        if (isset($usage["requests_left"]) && is_int($usage["requests_left"])) {
                            $this->requests_left = $usage["requests_left"];
                        }

                        if (isset($usage["credits_left"]) && is_int($usage["credits_left"])) {
                            $this->credits_left = $usage["credits_left"];
                        }
                    } else {
                        $error = ($json["error"] ?? array());

                        if (count($error) > 0) {
                            $error_encode = json_encode($error);
                            if (is_string($error_encode)) {
                                $log->backtrace($error_encode);
                            }
                        }
                    }
                } else {
                    $log->backtrace(json_last_error_msg());
                }
            } else {
                $log->backtrace($content);
            }
        } catch (Exception $exception) {
            $log = $log->exception($exception);
        }

        return $ret;
    }
}
