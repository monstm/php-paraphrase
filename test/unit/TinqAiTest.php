<?php

namespace Test\Unit;

use Samy\Paraphrase\TinqAi;

class TinqAiTest extends AbstractParaphrase
{
    protected function setUp(): void
    {
        $this->paraphrase = new TinqAi(TINQAI_APIKEY);
    }


    /**
     * @dataProvider \Test\Unit\TinqAiDataProvider::dataSupportedLanguages
     */
    public function testTinqAiSupportedLanguages($SupportedLanguages): void
    {
        $this->assertSame($SupportedLanguages, $this->paraphrase->getSupportedLanguages());
    }

    /**
     * @dataProvider \Test\Unit\TinqAiDataProvider::dataSupportedModes
     */
    public function testTinqAiSupportedModes($SupportedModes): void
    {
        $this->assertSame($SupportedModes, $this->paraphrase->getSupportedModes());
    }


    /**
     * @dataProvider \Test\Unit\TinqAiDataProvider::dataLanguage
     */
    public function testTinqAiLanguage($Language): void
    {
        $this->assertLanguage($Language);
    }

    /**
     * @dataProvider \Test\Unit\TinqAiDataProvider::dataMode
     */
    public function testTinqAiMode($Mode): void
    {
        $this->assertMode($Mode);
    }

    /**
     * @dataProvider \Test\Unit\TinqAiDataProvider::dataText
     */
    public function testTinqAiText($TextActual, $TextExpect): void
    {
        $this->assertText($TextActual, $TextExpect);
    }


    /**
     * @dataProvider \Test\Unit\TinqAiDataProvider::dataTier
     */
    public function testTinqAiTier($Tier): void
    {
        $this->assertInstanceOf(TinqAi::class, $this->paraphrase->withTier($Tier));
        $this->assertSame($Tier, $this->paraphrase->getTier());
    }

    /**
     * @dataProvider \Test\Unit\TinqAiDataProvider::dataUnique
     */
    public function testTinqAiUnique($Unique): void
    {
        $this->assertInstanceOf(TinqAi::class, $this->paraphrase->withUnique($Unique));
        $this->assertSame($Unique, $this->paraphrase->getUnique());
    }


    /**
     * @dataProvider \Test\Unit\TinqAiDataProvider::dataParaphraseEn
     */
    public function testTinqAiParaphrase($Text): void
    {
        $this->assertInstanceOf(
            TinqAi::class,
            $this->paraphrase
                ->withTier(TinqAi::TIER_FREE)
                ->withLanguage("en")
                ->withUnique(true)
                ->withMode(TinqAi::MODE_FLUENT)
                ->withText($Text)
        );

        $this->assertNotSame($Text, $this->paraphrase->paraphrase());
    }
}
