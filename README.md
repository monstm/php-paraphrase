# PHP Paraphrase

[
	![](https://badgen.net/packagist/v/samy/paraphrase/latest)
	![](https://badgen.net/packagist/license/samy/paraphrase)
	![](https://badgen.net/packagist/dt/samy/paraphrase)
	![](https://badgen.net/packagist/favers/samy/paraphrase)
](https://packagist.org/packages/samy/paraphrase)

A paraphrase is a restatement of the meaning of a text or passage using other words.

---

## Instalation

### Composer

Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

```sh
composer require samy/paraphrase
```

Please see [Composer](https://getcomposer.org/) for more information.

---

## Support

* Repository: <https://gitlab.com/monstm/php-paraphrase>
* Documentations: <https://monstm.gitlab.io/php-paraphrase/>
* Annotation: <https://monstm.alwaysdata.net/php-paraphrase/>
* Issues: <https://gitlab.com/monstm/php-paraphrase/-/issues>
