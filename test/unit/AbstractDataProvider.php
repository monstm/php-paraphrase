<?php

namespace Test\Unit;

use Samy\Dummy\Random;

abstract class AbstractDataProvider
{
    protected function csv(string $Basename): array
    {
        $ret = array();

        $filename = __DIR__ . DIRECTORY_SEPARATOR . $Basename . ".csv";

        if (is_file($filename)) {
            $file = fopen($filename, "r");

            if ($file) {
                while (($data = fgetcsv($file)) !== false) {
                    array_push($ret, $data);
                }

                fclose($file);
            }
        }

        return $ret;
    }


    // $Language
    public function dataLanguage(): array
    {
        $ret = array();
        $random = new Random();

        $count = $random->integer(5, 10);
        for ($index = 0; $index < $count; $index++) {
            array_push($ret, array($random->string()));
        }

        return $ret;
    }

    // $Mode
    public function dataMode(): array
    {
        $ret = array();
        $random = new Random();

        $count = $random->integer(5, 10);
        for ($index = 0; $index < $count; $index++) {
            array_push($ret, array($random->integer()));
        }

        return $ret;
    }

    // $TextActual, $TextExpect
    public function dataText(): array
    {
        $ret = array();

        foreach ($this->csv("text") as $data) {
            $count = count($data);

            array_push($ret, array(
                ($count > 0 ? $data[0] : 0),
                ($count > 1 ? $data[1] : "")
            ));
        }

        return $ret;
    }


    // $Text
    public function dataParaphraseEn(): array
    {
        $ret = array();

        $filename = __DIR__ . DIRECTORY_SEPARATOR . "paraphrase-en.txt";

        if (is_file($filename)) {
            array_push($ret, array(trim(@file_get_contents($filename))));
        }

        return $ret;
    }
}
