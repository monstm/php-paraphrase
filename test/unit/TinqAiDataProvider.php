<?php

namespace Test\Unit;

use Samy\Dummy\Random;
use Samy\Paraphrase\TinqAi;

class TinqAiDataProvider extends AbstractDataProvider
{
    // $SupportedLanguages
    public function dataSupportedLanguages(): array
    {
        return array(
            array(
                array(
                    array("code" => "en", "description" => "English"),
                    array("code" => "fr", "description" => "Francais"),
                    array("code" => "es", "description" => "Espanol"),
                    array("code" => "pt", "description" => "Portuguese")
                )
            )
        );
    }

    // $SupportedModes
    public function dataSupportedModes(): array
    {
        return array(
            array(
                array(
                    array(
                        "code" => TinqAi::MODE_NORMAL,
                        "description" => "The normal mode allows for simple rephrasing, with word changing."
                    ),
                    array(
                        "code" => TinqAi::MODE_STANDARD,
                        "description" => "The standard mode allows a relatively simple rephrasing" .
                            " with sentence structure changes while keeping the context of the input." .
                            " This is great for bypassing certain plagiarism checkers."
                    ),
                    array(
                        "code" => TinqAi::MODE_FLUENT,
                        "description" => "The fluent mode allows for more subtle rephrasing," .
                            " with more sophisticated words." .
                            " This is good for rewriting scientific or technical content."
                    ),
                    array(
                        "code" => TinqAi::MODE_CREATIVE,
                        "description" => "Creative content is great when rewriting blog posts" .
                            " or other work that may require less precision," .
                            " but more freedom for the A.I to make suggestions."
                    )
                )
            )
        );
    }


    // $Tier
    public function dataTier(): array
    {
        $ret = array();
        $random = new Random();

        $count = $random->integer(5, 10);
        for ($index = 0; $index < $count; $index++) {
            array_push($ret, array($random->integer(0, 4)));
        }

        return $ret;
    }

    // $Unique
    public function dataUnique(): array
    {
        $ret = array();
        $random = new Random();

        $count = $random->integer(5, 10);
        for ($index = 0; $index < $count; $index++) {
            array_push($ret, array($random->boolean()));
        }

        return $ret;
    }
}
