# Paraphrase Interface

Describes Paraphrase interface.

---

## withLanguage

Return an instance with provided language.

```php
$paraphrase = $paraphrase->withLanguage($language);
```

---

## getLanguage

Retrieve provided language.

```php
$language = $paraphrase->getLanguage();
```

---

## getSupportedLanguages

Retrieve supported languages.

```php
$languages = $paraphrase->getSupportedLanguages();
```

---

## withMode

Return an instance with provided mode.

```php
$paraphrase = $paraphrase->withMode($mode);
```

---

## getMode

Retrieve provided mode.

```php
$mode = $paraphrase->getMode();
```

---

## getSupportedModes

Retrieve supported modes.

```php
$modes = $paraphrase->getSupportedModes();
```

---

## withText

Return an instance with provided text.

```php
$paraphrase = $paraphrase->withText($text);
```

---

## getText

Retrieve provided text.

```php
$text = $paraphrase->getText();
```

---

## paraphrase

Retrieve paraphrased text.

```php
$paraphrased_text = $paraphrase->paraphrase();
```
